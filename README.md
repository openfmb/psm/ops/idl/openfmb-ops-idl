# OMG IDL definitions for OpenFMB operational use cases.

This repository contains the OMG IDL definitions based on the OpenFMB operational use case data model located [here](https://gitlab.com/openfmb/data-models/ops).

## How created

[Real Time Innovations](https://www.rti.com) created and Enterprise Architect plug-in that generates IDL based off an UML data model.  Here is a link to this plug-in and instructions for how to use it:

https://github.com/rticommunity/idl4-enterprise-architect

The IDL output from this tool should be vendor neutral so it can be used across any vendor that is OMG compliant.

## Using the IDL

Depending on the vendor that you are using, take the IDL from this repository and use it with that vendor's tools to generate language-specific bindings for you language of choice.

## Copyright

See the COPYRIGHT file for copyright information of information contained in this repository.

## License

Unless otherwise noted, all files in this repository are distributed under the Apache Version 2.0 license found in the LICENSE file.
